create table marathon
(
	id integer not null
		constraint marathon_pkey
			primary key,
	description varchar(255),
	end_date date,
	start_date date
);

create table "user"
(
	id integer not null
		constraint user_pkey
			primary key,
	picture_url varchar(255),
	name varchar(255),
	password varchar(255),
	role varchar(255),
	email varchar(256)
);

create table message
(
	id integer not null
		constraint message_pkey
			primary key,
	date_sent date,
	text varchar(255),
	user_id integer
		constraint fknebwitbhvl9nq6mqsdlmb0v75
			references "user",
	marathon_id integer,
	username varchar(255)
);

create table book
(
	id integer not null
		constraint book_pkey
			primary key,
	author varchar(255),
	description varchar(4096),
	image_link varchar(255),
	isbn varchar(255),
	page_amount integer,
	publication_year integer,
	title varchar(255)
);

create table user_book
(
	book_id integer not null
		constraint fk85pwltn867pjxog1gk5smtqcw
			references book,
	user_id integer not null,
	status integer,
	constraint user_book_pkey
		primary key (book_id, user_id)
);

create table note
(
	id integer not null
		constraint note_pkey
			primary key,
	book_id integer,
	text varchar(255),
	user_id integer,
	constraint fkbtefou7e8ig526pjr1odupkmm
		foreign key (book_id, user_id) references user_book
);

create table google_user
(
	id integer not null
		constraint google_user_pk
			primary key
		constraint google_user_user_id_fk
			references "user",
	user_id varchar(256),
	email varchar(256),
	email_verified boolean,
	name varchar(256),
	picture_url varchar(256),
	locale varchar(256),
	family_name varchar(256),
	given_name varchar(256)
);

create table facebook_user
(
	id integer not null
		constraint facebook_user_pk
			primary key
		constraint facebook_user_user_id_fk
			references "user",
	user_id varchar(255),
	email varchar(255),
	name varchar(255)
);

create table vk_user
(
	id integer not null
		constraint vk_user_pk
			primary key
		constraint vk_user_user_id_fk
			references "user",
	user_id varchar(256),
	name varchar(256)
);

create table refresh_token
(
	id serial not null
		constraint refresh_token_pk
			primary key,
	user_id integer not null
		constraint refresh_token_user_id_fk
			references "user"
				on update cascade on delete cascade,
	ip varchar(255),
	os varchar(255),
	browser varchar(255),
	user_agent varchar(255),
	token varchar(255) not null,
	expired_at timestamp not null,
	created_at timestamp not null,
	updated_at timestamp not null
);