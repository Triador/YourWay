package com.triador.yourwayserver.controller;

import com.triador.yourwayserver.dao.model.Note;
import com.triador.yourwayserver.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"/notes"})
public class NoteController {

    private NoteService noteService;

    @Autowired
    public NoteController(NoteService noteService) {
        this.noteService = noteService;
    }

    @PostMapping
    public Note saveNote(@RequestBody Note note) {
        return noteService.save(note);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteNote(@PathVariable Integer id) {
        noteService.delete(id);
    }

}
