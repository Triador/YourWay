package com.triador.yourwayserver.controller;

import com.triador.yourwayserver.dto.request.MessageRequest;
import com.triador.yourwayserver.dto.response.MessageResponse;
import com.triador.yourwayserver.service.ChatService;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"chat"})
public class ChatController {

    private ChatService chatService;

    public ChatController(ChatService chatService) {
        this.chatService = chatService;
    }

    @MessageMapping("message")
    @SendTo("/topic/message")
    public MessageResponse sendMessage(MessageRequest messageRequest) {
        return chatService.saveMessage(messageRequest);
    }

    @GetMapping(value = "/messages/{marathonId}")
    public List<MessageResponse> getMarathonMessages(@PathVariable Integer marathonId) {
        return chatService.getMessages(marathonId);
    }
}
