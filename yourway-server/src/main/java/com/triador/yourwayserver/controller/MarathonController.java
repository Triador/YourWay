package com.triador.yourwayserver.controller;

import com.triador.yourwayserver.dao.model.Marathon;
import com.triador.yourwayserver.service.MarathonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"marathons"})
public class MarathonController {

    private MarathonService marathonService;

    @Autowired
    public MarathonController(MarathonService marathonService) {
        this.marathonService = marathonService;
    }

    @PostMapping
    public Marathon saveMarathon(@RequestBody Marathon marathon) {
        return marathonService.saveMarathon(marathon);
    }

    @GetMapping
    public List<Marathon> getMarathons() {
        return marathonService.getMarathons();
    }

    @GetMapping(path = {"/{id}"})
    public Marathon getMarathon(@PathVariable int id) {
        return marathonService.getMarathon(id);
    }
}
