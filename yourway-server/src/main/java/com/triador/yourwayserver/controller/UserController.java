package com.triador.yourwayserver.controller;

import com.triador.yourwayserver.dao.model.User;
import com.triador.yourwayserver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"users"})
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/signup")
    public User createUser(@RequestBody User user) {
        return userService.save(user);
    }

    @PostMapping

    @GetMapping(path = {"/{id}"})
    public User getUser(@PathVariable("id") int userId) {
        return userService.findById(userId);
    }

}
