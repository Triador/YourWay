package com.triador.yourwayserver.controller;

import com.triador.yourwayserver.dto.response.BookResponse;
import com.triador.yourwayserver.dto.response.BookTitleResponse;
import com.triador.yourwayserver.dto.response.ShortBookDescriptionResponse;
import com.triador.yourwayserver.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"books"})
public class BookController {

    private BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping(path = {"/{id}"})
    public BookResponse getBook(@PathVariable Integer id) {
        return bookService.getBook(id);
    }

    @GetMapping(path = {"/search/{titlePart}"})
    public List<BookTitleResponse> searchTitles(@PathVariable String titlePart) {
        return bookService.getBookTitles(titlePart);
    }

    @GetMapping
    public List<ShortBookDescriptionResponse> getBooks() {
        return bookService.getShortBookDescriptions();
    }
}
