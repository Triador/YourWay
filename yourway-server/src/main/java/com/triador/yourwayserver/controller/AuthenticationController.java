package com.triador.yourwayserver.controller;

import com.triador.yourwayserver.dto.Jwt;
import com.triador.yourwayserver.dto.request.LoginUserRequest;
import com.triador.yourwayserver.service.authentication.FacebookAuthenticationService;
import com.triador.yourwayserver.service.authentication.GoogleAuthenticationService;
import com.triador.yourwayserver.service.authentication.SimpleAuthenticationService;
import com.triador.yourwayserver.service.authentication.VKAuthenticationService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    private SimpleAuthenticationService simpleAuthenticationService;
    private GoogleAuthenticationService googleAuthenticationService;
    private FacebookAuthenticationService facebookAuthenticationService;
    private VKAuthenticationService vkAuthenticationService;

    public AuthenticationController(SimpleAuthenticationService simpleAuthenticationService,
                                    GoogleAuthenticationService googleAuthenticationService,
                                    FacebookAuthenticationService facebookAuthenticationService,
                                    VKAuthenticationService vkAuthenticationService) {
        this.simpleAuthenticationService = simpleAuthenticationService;
        this.googleAuthenticationService = googleAuthenticationService;
        this.facebookAuthenticationService = facebookAuthenticationService;
        this.vkAuthenticationService = vkAuthenticationService;
    }

    //todo test this method try to enter incorrect password
    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@RequestBody LoginUserRequest loginUserRequest) throws AuthenticationException {
        Jwt jwt = simpleAuthenticationService.generateToken(loginUserRequest);
        return ResponseEntity.ok(jwt);
    }

    @PostMapping(value = "/google-login")
    public ResponseEntity<?> googleLogin(@RequestBody Jwt googleToken) {
        Jwt jwt = googleAuthenticationService.generateToken(googleToken.getAccessToken());
        return ResponseEntity.ok(jwt);
    }

    @PostMapping(value = "/facebook-login")
    public ResponseEntity<?> facebookLogin(@RequestBody Jwt facebookToken) {
        Jwt jwt = facebookAuthenticationService.generateToken(facebookToken.getAccessToken());
        return ResponseEntity.ok(jwt);
    }

    @PostMapping(value = "/vk-login")
    public ResponseEntity<?> vkLogin(@RequestBody Jwt vkToken) {
        Jwt jwt = vkAuthenticationService.generateToken(vkToken);
        return ResponseEntity.ok(jwt);
    }

    @PostMapping(value = "/refresh")
    public ResponseEntity<?> refreshAccessToken(@RequestBody Jwt jwtRequest) {
        System.out.println(jwtRequest.getAccessToken() + "    " + jwtRequest.getRefreshToken());
        Jwt jwtResponse = simpleAuthenticationService.refreshAccessToken(jwtRequest);
        return ResponseEntity.ok(jwtResponse);
    }

    @PostMapping(value = "/logout")
    public ResponseEntity<?> logout(@RequestBody Jwt jwtRequest) {
        System.out.println("aaaaaaAA");
        Integer deleted = simpleAuthenticationService.logout(jwtRequest);
        return ResponseEntity.ok(deleted);
    }
}
