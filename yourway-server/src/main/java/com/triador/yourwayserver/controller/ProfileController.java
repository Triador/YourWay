package com.triador.yourwayserver.controller;

import com.triador.yourwayserver.dto.request.AddUserBookRequest;
import com.triador.yourwayserver.dto.response.ProfileResponse;
import com.triador.yourwayserver.service.ProfileService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"profiles"})
public class ProfileController {

    private ProfileService profileService;

    public ProfileController(ProfileService profileService) {
        this.profileService = profileService;
    }

    @PostMapping()
    public void addBookToProfile(@RequestBody AddUserBookRequest addUserBookRequest) {
        profileService.saveBook(addUserBookRequest);
    }

    @GetMapping(path = {"/{userId}"})
    public ProfileResponse getProfile(@PathVariable String userId) {
        return profileService.findById(Integer.parseInt(userId));
    }

    @DeleteMapping()
    public void deleteBook(@RequestParam(name = "uid") Integer userId,
                           @RequestParam(name= "bookId") Integer bookId) {
        profileService.deleteBook(userId, bookId);
    }
}
