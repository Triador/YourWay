package com.triador.yourwayserver.utils;

import com.triador.yourwayserver.dao.model.Book;
import com.triador.yourwayserver.dao.repo.BookRepository;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Lazy
public class ChitaiGorodParser {

    private WebDriver driver = SeleniumUtils.getDriver();
    private BookRepository bookRepository;
    private Converter converter;

    @Autowired
    public ChitaiGorodParser(BookRepository bookRepository, Converter converter) {
        this.bookRepository = bookRepository;
        this.converter = converter;
    }

    public void parse(List<String> urls) {
        for (String url : urls) {
            driver.get(url);
//            sleep(100);
//            scrollDown();
            int pagesCount = Integer.parseInt(driver.findElement(By.className("pagination-item:nth-child(10)")).getText());
            System.out.println(pagesCount + " -----------------------------------------------------------------------");
            for (int pageId = 1; pageId <= pagesCount; pageId++){
                driver.get(url+"?page="+pageId);
                driver.findElement(By.className("toggle-cardview__item_port")).click();
                List<WebElement> books = driver.findElements(By.className("product-card"));
                List<WebElement> smallBookImages = driver.findElements(By.cssSelector("a.product-card__img > img"));
                List<WebElement> bookTitles = driver.findElements(By.className("product-card__title"));

                for (int i = 0; i < smallBookImages.size(); i++) {
                    String imageUrl = smallBookImages.get(i).getAttribute("data-original");
                    String extension = getImageExtension(imageUrl);
                    String bookTitle = bookTitles.get(i).getText();
                    downloadBookImage(imageUrl, "small_" + converter.convert(bookTitle), extension);
                }
                saveBooks(books);
            }
            driver.close();
        }
    }

    private void saveBooks(List<WebElement> webElements) {
        int id = 0;
        for (WebElement webElement : webElements) {
            String bookUrl = webElement.findElement(By.className("product-card__img")).getAttribute("href");
            System.out.println(++id + " -----------------------------------------------------------------------");
            Book book = getBook(bookUrl);
//            sleep(1000);
            try {
                bookRepository.save(book);
            } catch(DataIntegrityViolationException exception){}

            System.out.println("book " + book.getTitle() + " saved");
        }
    }

    private Book getBook(String bookUrl) {
        ArrayList<String> tabs = openNewBookWindow(bookUrl);

        List<WebElement> bookParametersValues = driver.findElements(By.className("product-prop__value"));
        List<WebElement> bookParameters = driver.findElements(By.className("product-prop__title"));
        Map<String, String> parametersMap = new HashMap<>();
        for (int i = 0; i < bookParameters.size(); i++) {
            parametersMap.put(bookParameters.get(i).getText(), bookParametersValues.get(i).getText());
        }
        Book bookResponse = mapBook(parametersMap);

        String imageUrl = driver.findElement(By.cssSelector("img[itemprop='image']"))
                .getAttribute("src");
        if (!imageUrl.isEmpty()) {
            String extension = getImageExtension(imageUrl);
            bookResponse.setImageLink(converter.convert(bookResponse.getTitle()) + extension);

            downloadBookImage(imageUrl, "big_" + converter.convert(bookResponse.getTitle()), extension);
        }

        driver.close();
        driver.switchTo().window(tabs.get(0));

        return bookResponse;
    }

    private Book mapBook(Map<String, String> parametersMap) {
        String title = driver.findElement(By.className("product__title")).getText();
        String author = driver.findElement(By.className("product__author")).getText();

        int publicationYear = Integer.parseInt(parametersMap.getOrDefault("Год издания", "0"));
        int pageAmount = Integer.parseInt(parametersMap.getOrDefault("Кол-во страниц", "0"));
        String isbns = parametersMap.getOrDefault("ISBN", "undefined");

        String description = driver.findElement(By.cssSelector("div[itemprop='description']")).getText();

        Book bookResponse = new Book();
        bookResponse.setTitle(title);
        bookResponse.setAuthor(author);
        bookResponse.setPageAmount(pageAmount);
        bookResponse.setPublicationYear(publicationYear);
        bookResponse.setIsbn(isbns);
        bookResponse.setDescription(description);

        return bookResponse;
    }

    private void scrollDown() {
        for (int i = 0; i < 3; i++) {
            ((JavascriptExecutor) driver)
                    .executeScript("window.scrollTo(0, document.body.scrollHeight)");
            try {
                driver.findElement(By.className("js__show-more-cards")).click();
            } catch (ElementNotVisibleException ignored) {}
            sleep(1000);
        }
    }

    private int getScrollsAmount() {
        String text = driver.findElement(By.className("count-result__value")).getText();
        int amount = Integer.parseInt(text);
        return amount / 20;
    }

    private ArrayList<String> openNewBookWindow(String bookUrl) {
        ((JavascriptExecutor) driver).executeScript("window.open()");
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        driver.get(bookUrl);
//        sleep(1000);

        return tabs;
    }

    private void downloadBookImage(String url, String bookTitle, String extension) {
        try (InputStream in = new URL(url).openStream()) {
            Files.copy(in, Paths.get("G:\\JavaProjects\\YourWay\\yourway-client\\src\\assets\\book_images\\" + bookTitle + extension));
//            Files.copy(in, Paths.get("C:\\Users\\aandreev\\Workspace\\YourWay\\yourway-client\\src\\assets\\book_images\\" + bookTitle + extension));
//            Files.copy(in, Paths.get("/Users/pro100skm/Documents/booker/YourWay/yourway-client/src/assets/book_images/" + bookTitle + extension));
        } catch (FileAlreadyExistsException e) {
            System.out.println("this image has been already downloaded");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String getImageExtension(String imageUrl) {
        return imageUrl.substring(imageUrl.lastIndexOf("."));
    }
}
