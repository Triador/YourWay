package com.triador.yourwayserver.service.authentication;

import com.triador.yourwayserver.dao.model.SocialUser;
import com.triador.yourwayserver.dao.repo.VKUserRepository;
import com.triador.yourwayserver.dto.Jwt;
import com.triador.yourwayserver.exception.CommonException;
import com.triador.yourwayserver.service.converter.VKProfileToVKUserConverter;
import com.triador.yourwayserver.service.converter.VKUserToUserConverter;
import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.users.UserXtrCounters;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VKAuthenticationService {

    private SimpleAuthenticationService simpleAuthenticationService;
    private VKProfileToVKUserConverter vkProfileToVKUserConverter;
    private VKUserToUserConverter vkUserToUserConverter;
    private VKUserRepository vkUserRepository;

    public VKAuthenticationService(SimpleAuthenticationService simpleAuthenticationService,
                                   VKProfileToVKUserConverter vkProfileToVKUserConverter,
                                   VKUserToUserConverter vkUserToUserConverter,
                                   VKUserRepository vkUserRepository) {
        this.simpleAuthenticationService = simpleAuthenticationService;
        this.vkProfileToVKUserConverter = vkProfileToVKUserConverter;
        this.vkUserToUserConverter = vkUserToUserConverter;
        this.vkUserRepository = vkUserRepository;
    }

    public Jwt generateToken(Jwt jwt) {
        TransportClient transportClient = HttpTransportClient.getInstance();
        VkApiClient vk = new VkApiClient(transportClient);
        UserActor userActor = new UserActor(jwt.getUserId(), jwt.getAccessToken());
        List<UserXtrCounters> userOptions;
        try {
            userOptions = vk.users().get(userActor).execute();
        } catch (ClientException | ApiException e) {
            throw new CommonException("something went wrong during vk authentication");
        }
        UserXtrCounters userXtrCounters = userOptions.get(0);
        SocialUser socialUser = vkProfileToVKUserConverter.convert(userXtrCounters);

        return simpleAuthenticationService.generateToken(socialUser, vkUserRepository, vkUserToUserConverter);
    }
}
