package com.triador.yourwayserver.service.authentication;

import com.triador.yourwayserver.dao.model.SocialUser;
import com.triador.yourwayserver.dao.model.User;
import com.triador.yourwayserver.dao.repo.SocialUserRepository;
import com.triador.yourwayserver.dto.Jwt;
import com.triador.yourwayserver.dto.request.LoginUserRequest;
import com.triador.yourwayserver.jwt.JwtTokenService;
import com.triador.yourwayserver.service.UserService;
import com.triador.yourwayserver.service.converter.Converter;
import org.springframework.stereotype.Service;

@Service
public class SimpleAuthenticationService {

    private JwtTokenService jwtTokenService;
    private UserService userService;

    public SimpleAuthenticationService(JwtTokenService jwtTokenService,
                                       UserService userService) {
        this.jwtTokenService = jwtTokenService;
        this.userService = userService;
    }

    public Jwt generateToken(LoginUserRequest loginUserRequest) {
        User user = userService.findByEmail(loginUserRequest.getEmail());
        return generateToken(user.getId());
    }

    public Jwt generateToken(SocialUser socialUser, SocialUserRepository socialUserRepository, Converter<SocialUser, User> converter) {
        if (socialUserRepository.existsByUserId(socialUser.getUserId())) {
            socialUser.setId(socialUserRepository.getIdByUserId(socialUser.getUserId()));
        } else {
            User user = userService.save(converter.convert(socialUser));
            socialUser.setId(user.getId());
            socialUserRepository.save(socialUser);
        }
        return generateToken(socialUser.getId());
    }

    private Jwt generateToken(Integer userId) {
        User user = userService.findById(userId);
        String accessToken = jwtTokenService.generateToken(user);
        String refreshToken = jwtTokenService.generateRefreshToken(userId);
        return new Jwt(accessToken, refreshToken);
    }

    public Jwt refreshAccessToken(Jwt jwtRequest) {
        return jwtTokenService.refreshAccessToken(jwtRequest);
    }

    public Integer logout(Jwt jwt) {
        return jwtTokenService.deleteRefreshToken(jwt.getRefreshToken());
    }
}
