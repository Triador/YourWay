package com.triador.yourwayserver.service.converter;

import com.triador.yourwayserver.dao.model.FacebookUser;
import com.triador.yourwayserver.dao.model.SocialUser;
import com.triador.yourwayserver.dao.model.User;
import com.triador.yourwayserver.utils.SecurityRoleConstants;
import org.springframework.stereotype.Service;

@Service
public class FacebookUserToUserConverter implements Converter<SocialUser, User> {

    @Override
    public User convert(SocialUser socialUser) {
        FacebookUser facebookUser = (FacebookUser) socialUser;
        User user = new User();
        user.setName(facebookUser.getName());
        user.setEmail(facebookUser.getEmail());
        //todo this will be added in separate task
        user.setPictureUrl(null);
        //todo decide what to do with password for users from social login
        user.setPassword("social-login");
        user.setRole(SecurityRoleConstants.USER);
        return user;
    }
}
