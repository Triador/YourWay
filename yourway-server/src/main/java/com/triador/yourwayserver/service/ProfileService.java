package com.triador.yourwayserver.service;

import com.triador.yourwayserver.dao.model.User;
import com.triador.yourwayserver.dao.model.UserBook;
import com.triador.yourwayserver.dao.model.UserBookKey;
import com.triador.yourwayserver.dao.repo.UserBookRepository;
import com.triador.yourwayserver.dao.repo.UserRepository;
import com.triador.yourwayserver.dto.request.AddUserBookRequest;
import com.triador.yourwayserver.dto.response.BookResponse;
import com.triador.yourwayserver.dto.response.ProfileResponse;
import com.triador.yourwayserver.enumeration.ErrorMessage;
import com.triador.yourwayserver.exception.CommonException;
import com.triador.yourwayserver.service.converter.BookToBookResponseConverter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProfileService {

    private UserBookRepository userBookRepository;
    private UserRepository userRepository;
    private BookToBookResponseConverter bookToBookResponseConverter;

    public ProfileService(UserBookRepository userBookRepository,
                          UserRepository userRepository,
                          BookToBookResponseConverter bookToBookResponseConverter) {
        this.userBookRepository = userBookRepository;
        this.userRepository = userRepository;
        this.bookToBookResponseConverter = bookToBookResponseConverter;
    }

    public void saveBook(AddUserBookRequest addUserBookRequest) {
        UserBookKey userBookKey = new UserBookKey(addUserBookRequest.getUserId(), addUserBookRequest.getBookId());
        UserBook userBook = new UserBook();
        userBook.setId(userBookKey);
        userBook.setStatus(addUserBookRequest.getBookStatus());
        userBookRepository.save(userBook);
    }

    public ProfileResponse findById(Integer userId) {
        User user = userRepository.findById(userId).orElseThrow(()
                -> new CommonException(ErrorMessage.ERROR_NOT_FOUND.getMessage()));
        ProfileResponse.ProfileBuilder profileBuilder = ProfileResponse.builder();
        profileBuilder
                .username(user.getName())
                .imageLink(user.getPictureUrl());

        List<UserBook> fullBookInfoList = userBookRepository.findByIdUserId(userId);

        List<BookResponse> progressBookResponses = new ArrayList<>();
        List<BookResponse> futureBookResponses = new ArrayList<>();
        List<BookResponse> finishedBookResponses = new ArrayList<>();

        for (UserBook fullBookInfo : fullBookInfoList) {
            BookResponse bookResponse = bookToBookResponseConverter.convert(fullBookInfo.getBook());
            bookResponse.setNotes(fullBookInfo.getNotes());
            bookResponse.setStatus(fullBookInfo.getStatus());

            switch (fullBookInfo.getStatus()) {
                case PROGRESS: {
                    progressBookResponses.add(bookResponse);
                    break;
                }
                case FUTURE: {
                    futureBookResponses.add(bookResponse);
                    break;
                }
                case FINISHED: {
                    finishedBookResponses.add(bookResponse);
                }
            }
        }

        profileBuilder.progressBooks(progressBookResponses);
        profileBuilder.futureBooks(futureBookResponses);
        profileBuilder.finishedBooks(finishedBookResponses);

        return profileBuilder.build();
    }

    public void deleteBook(Integer userId, Integer bookId) {
        userBookRepository.deleteById(new UserBookKey(userId, bookId));
    }
}
