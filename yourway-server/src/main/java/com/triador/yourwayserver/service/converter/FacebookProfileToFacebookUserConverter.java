package com.triador.yourwayserver.service.converter;

import com.triador.yourwayserver.dao.model.FacebookUser;
import org.springframework.social.facebook.api.User;
import org.springframework.stereotype.Service;

@Service
public class FacebookProfileToFacebookUserConverter implements Converter<User, FacebookUser> {

    @Override
    public FacebookUser convert(User userProfile) {
        FacebookUser facebookUser = new FacebookUser();
        facebookUser.setUserId(userProfile.getId());
        facebookUser.setEmail(userProfile.getEmail());
        facebookUser.setName(userProfile.getName());
        return facebookUser;
    }
}
