package com.triador.yourwayserver.service.authentication;

import com.triador.yourwayserver.dao.model.SocialUser;
import com.triador.yourwayserver.dao.repo.FacebookUserRepository;
import com.triador.yourwayserver.dto.Jwt;
import com.triador.yourwayserver.service.converter.FacebookProfileToFacebookUserConverter;
import com.triador.yourwayserver.service.converter.FacebookUserToUserConverter;
import org.springframework.social.connect.Connection;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.stereotype.Service;

@Service
public class FacebookAuthenticationService {

    private FacebookConnectionFactory facebookConnectionFactory;
    private FacebookUserRepository facebookUserRepository;
    private FacebookProfileToFacebookUserConverter facebookProfileToFacebookUserConverter;
    private FacebookUserToUserConverter facebookUserToUserConverter;
    private SimpleAuthenticationService simpleAuthenticationService;

    public FacebookAuthenticationService(FacebookConnectionFactory facebookConnectionFactory,
                                         FacebookUserRepository facebookUserRepository,
                                         FacebookProfileToFacebookUserConverter facebookProfileToFacebookUserConverter,
                                         FacebookUserToUserConverter facebookUserToUserConverter,
                                         SimpleAuthenticationService simpleAuthenticationService) {
        this.facebookConnectionFactory = facebookConnectionFactory;
        this.facebookUserRepository = facebookUserRepository;
        this.facebookProfileToFacebookUserConverter = facebookProfileToFacebookUserConverter;
        this.facebookUserToUserConverter = facebookUserToUserConverter;
        this.simpleAuthenticationService = simpleAuthenticationService;
    }

    public Jwt generateToken(String accessToken) {
        AccessGrant accessGrant = new AccessGrant(accessToken);
        Connection<Facebook> connection = facebookConnectionFactory.createConnection(accessGrant);
        Facebook facebook = connection.getApi();
        String[] fields = { "id", "email", "name" };
        SocialUser socialUser = facebookProfileToFacebookUserConverter.convert(
                facebook.fetchObject("me", org.springframework.social.facebook.api.User.class, fields));

        return simpleAuthenticationService.generateToken(socialUser, facebookUserRepository, facebookUserToUserConverter);
    }
}
