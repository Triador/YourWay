package com.triador.yourwayserver.service.converter;

import com.triador.yourwayserver.dao.model.SocialUser;
import com.triador.yourwayserver.dao.model.User;
import com.triador.yourwayserver.dao.model.VKUser;
import com.triador.yourwayserver.utils.SecurityRoleConstants;
import org.springframework.stereotype.Service;

@Service
public class VKUserToUserConverter implements Converter<SocialUser, User> {

    @Override
    public User convert(SocialUser socialUser) {
        VKUser vkUser = (VKUser) socialUser;
        User user = new User();
        user.setName(vkUser.getName());
        //todo decide what to do with password for users from social login
        user.setPassword("social-login");
        user.setRole(SecurityRoleConstants.USER);
        return user;
    }
}
