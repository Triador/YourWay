package com.triador.yourwayserver.service.converter;

import com.triador.yourwayserver.dao.model.VKUser;
import com.vk.api.sdk.objects.users.UserXtrCounters;
import org.springframework.stereotype.Service;

@Service
public class VKProfileToVKUserConverter implements Converter<UserXtrCounters, VKUser> {

    @Override
    public VKUser convert(UserXtrCounters userProfile) {
        VKUser user = new VKUser();
        user.setUserId(String.valueOf(userProfile.getId()));
        user.setName(userProfile.getFirstName() + " " + userProfile.getLastName());
        return user;
    }
}
