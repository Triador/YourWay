package com.triador.yourwayserver.service.converter;

import com.triador.yourwayserver.dao.model.GoogleUser;
import com.triador.yourwayserver.dao.model.SocialUser;
import com.triador.yourwayserver.dao.model.User;
import com.triador.yourwayserver.utils.SecurityRoleConstants;
import org.springframework.stereotype.Service;

@Service
public class GoogleUserToUserConverter implements Converter<SocialUser, User> {

    @Override
    public User convert(SocialUser socialUser) {
        GoogleUser googleUser = (GoogleUser) socialUser;
        User user = new User();
        user.setName(googleUser.getName());
        user.setEmail(googleUser.getEmail());
        user.setPictureUrl(googleUser.getPictureUrl());
        //todo decide what to do with password for users from social login
        user.setPassword("social-login");
        user.setRole(SecurityRoleConstants.USER);
        return user;
    }
}
