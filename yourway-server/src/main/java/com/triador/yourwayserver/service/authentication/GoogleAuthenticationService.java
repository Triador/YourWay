package com.triador.yourwayserver.service.authentication;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.triador.yourwayserver.dao.model.SocialUser;
import com.triador.yourwayserver.dao.repo.GoogleUserRepository;
import com.triador.yourwayserver.dto.Jwt;
import com.triador.yourwayserver.exception.CommonException;
import com.triador.yourwayserver.service.converter.GooglePayloadToGoogleUserConverter;
import com.triador.yourwayserver.service.converter.GoogleUserToUserConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

@Service
public class GoogleAuthenticationService {

    @Value("${spring.security.oauth2.client.registration.google.clientId}")
    private String googleClientId;

    private GoogleUserRepository googleUserRepository;
    private GoogleUserToUserConverter googleUserToUserConverter;
    private GooglePayloadToGoogleUserConverter googlePayloadToGoogleUserConverter;
    private SimpleAuthenticationService simpleAuthenticationService;

    public GoogleAuthenticationService(GoogleUserRepository googleUserRepository,
                                       GoogleUserToUserConverter googleUserToUserConverter,
                                       GooglePayloadToGoogleUserConverter googlePayloadToGoogleUserConverter,
                                       SimpleAuthenticationService simpleAuthenticationService) {
        this.googleUserRepository = googleUserRepository;
        this.googleUserToUserConverter = googleUserToUserConverter;
        this.googlePayloadToGoogleUserConverter = googlePayloadToGoogleUserConverter;
        this.simpleAuthenticationService = simpleAuthenticationService;
    }

    public Jwt generateToken(String tokenId) {
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), new JacksonFactory())
                .setAudience(Collections.singletonList(googleClientId))
                .build();

        GoogleIdToken idToken;
        try {
            idToken = verifier.verify(tokenId);
        } catch (GeneralSecurityException | IOException e) {
            throw new CommonException("something went wrong during google authentication", e);
        }
        if (idToken != null) {
            GoogleIdToken.Payload payload = idToken.getPayload();
            SocialUser socialUser = googlePayloadToGoogleUserConverter.convert(payload);
            return simpleAuthenticationService.generateToken(socialUser, googleUserRepository, googleUserToUserConverter);
        } else {
            throw new CommonException("something went wrong during google authentication");
        }
    }
}
