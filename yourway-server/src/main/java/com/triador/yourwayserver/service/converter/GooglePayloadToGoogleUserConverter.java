package com.triador.yourwayserver.service.converter;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.triador.yourwayserver.dao.model.GoogleUser;
import org.springframework.stereotype.Service;

@Service
public class GooglePayloadToGoogleUserConverter implements Converter<Payload, GoogleUser> {

    @Override
    public GoogleUser convert(Payload payload) {
        GoogleUser googleUser = new GoogleUser();
        googleUser.setUserId(payload.getSubject());
        googleUser.setEmail(payload.getEmail());
        googleUser.setEmailVerified(payload.getEmailVerified());
        googleUser.setName((String) payload.get("name"));
        googleUser.setPictureUrl((String) payload.get("picture"));
        googleUser.setLocale((String) payload.get("locale"));
        googleUser.setFamilyName((String) payload.get("family_name"));
        googleUser.setGivenName((String) payload.get("given_name"));
        return googleUser;
    }
}
