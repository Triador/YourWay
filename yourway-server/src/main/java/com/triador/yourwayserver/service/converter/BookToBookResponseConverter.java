package com.triador.yourwayserver.service.converter;

import com.triador.yourwayserver.dao.model.Book;
import com.triador.yourwayserver.dto.response.BookResponse;
import org.springframework.stereotype.Service;

@Service
public class BookToBookResponseConverter implements Converter<Book, BookResponse> {

    @Override
    public BookResponse convert(Book book) {
        return BookResponse.builder()
                .id(book.getId())
                .title(book.getTitle())
                .author(book.getAuthor())
                .pageAmount(book.getPageAmount())
                .publicationYear(book.getPublicationYear())
                .isbn(book.getIsbn())
                .description(book.getDescription())
                .imageLink(book.getImageLink())
                .build();
    }
}
