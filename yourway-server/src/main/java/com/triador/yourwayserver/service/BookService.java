package com.triador.yourwayserver.service;

import com.triador.yourwayserver.dao.model.Book;
import com.triador.yourwayserver.dao.model.UserBook;
import com.triador.yourwayserver.dao.model.UserBookKey;
import com.triador.yourwayserver.dao.repo.BookRepository;
import com.triador.yourwayserver.dao.repo.UserBookRepository;
import com.triador.yourwayserver.dto.response.BookResponse;
import com.triador.yourwayserver.dto.response.BookTitleResponse;
import com.triador.yourwayserver.dto.response.ShortBookDescriptionResponse;
import com.triador.yourwayserver.enumeration.BookStatus;
import com.triador.yourwayserver.exception.CommonException;
import com.triador.yourwayserver.jwt.SecurityContext;
import com.triador.yourwayserver.service.converter.BookToBookResponseConverter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    private BookRepository bookRepository;
    private UserBookRepository userBookRepository;
    private BookToBookResponseConverter bookToBookResponseConverter;

    public BookService(BookRepository bookRepository,
                       UserBookRepository userBookRepository,
                       BookToBookResponseConverter bookToBookResponseConverter) {
        this.bookRepository = bookRepository;
        this.userBookRepository = userBookRepository;
        this.bookToBookResponseConverter = bookToBookResponseConverter;
    }

    public Book saveBook(Book book) {
        return bookRepository.save(book);
    }

    public List<ShortBookDescriptionResponse> getShortBookDescriptions() {
        return bookRepository.retrieveShortBookDescriptions();
    }

    public BookResponse getBook(Integer bookId) {
        UserBookKey userBookKey = new UserBookKey(SecurityContext.getTokenDetails().getUserId(), bookId);
        Optional<UserBook> fullBookInfo = userBookRepository.findById(userBookKey);
        BookResponse bookResponse;
        if (fullBookInfo.isPresent()) {
            bookResponse = bookToBookResponseConverter.convert(fullBookInfo.get().getBook());
            bookResponse.setStatus(fullBookInfo.get().getStatus());
            bookResponse.setNotes(fullBookInfo.get().getNotes());
        } else {
            Book book = bookRepository.findById(bookId)
                    .orElseThrow(() -> new CommonException("book not found"));
            bookResponse = bookToBookResponseConverter.convert(book);
            bookResponse.setStatus(BookStatus.UNLOCK);
        }

        return bookResponse;
    }

    public List<BookTitleResponse> getBookTitles(String titlePart) {
        return bookRepository.retrieveBookTitles(titlePart);
    }
}

