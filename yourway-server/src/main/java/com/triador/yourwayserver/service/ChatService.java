package com.triador.yourwayserver.service;

import com.triador.yourwayserver.dao.model.Message;
import com.triador.yourwayserver.dao.repo.ChatMessageRepository;
import com.triador.yourwayserver.dto.request.MessageRequest;
import com.triador.yourwayserver.dto.response.MessageResponse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ChatService {

    private ChatMessageRepository chatMessageRepository;

    public ChatService(ChatMessageRepository chatMessageRepository) {
        this.chatMessageRepository = chatMessageRepository;
    }

    public MessageResponse saveMessage(MessageRequest messageRequest) {
        Message message = new Message();
        message.setMarathonId(messageRequest.getMarathonId());
        message.setUserId(messageRequest.getUserId());
        message.setUsername(messageRequest.getUsername());
        message.setText(messageRequest.getText());
        message.setDateSent(messageRequest.getDateSent());
        chatMessageRepository.save(message);

        String text = messageRequest.getUsername() + ": " + messageRequest.getText();
        return new MessageResponse(text, messageRequest.getDateSent());
    }

    public List<MessageResponse> getMessages(Integer marathonId) {
        List<Message> messages = chatMessageRepository.getAllByMarathonId(marathonId);
        return messages.stream()
                .map(message -> {
                    String text = message.getUsername() + ": " + message.getText();
                    return new MessageResponse(text, message.getDateSent());
                })
                .collect(Collectors.toList());
    }
}
