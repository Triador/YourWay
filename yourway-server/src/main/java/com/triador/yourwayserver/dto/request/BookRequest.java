package com.triador.yourwayserver.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookRequest {

    private int userId;
    private int bookId;
}
