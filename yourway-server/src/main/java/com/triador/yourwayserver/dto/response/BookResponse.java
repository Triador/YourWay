package com.triador.yourwayserver.dto.response;

import com.triador.yourwayserver.dao.model.Note;
import com.triador.yourwayserver.enumeration.BookStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder(builderClassName = "Builder")
public class BookResponse {

    private Integer id;
    private String title;
    private String author;
    private Integer pageAmount;
    private Integer publicationYear;
    private String isbn;
    private String description;
    private String imageLink;
    private BookStatus status;
    private List<Note> notes;
}
