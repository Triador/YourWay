package com.triador.yourwayserver.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class MessageRequest {

    private Integer userId;
    private Integer marathonId;
    private String username;
    private String text;
    private LocalDateTime dateSent;
}
