package com.triador.yourwayserver.dto.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder(builderClassName = "ProfileBuilder")
public class ProfileResponse {

    private String username;
    private String imageLink;
    private List<BookResponse> progressBooks;
    private List<BookResponse> futureBooks;
    private List<BookResponse> finishedBooks;
}
