package com.triador.yourwayserver.jwt;

import com.triador.yourwayserver.dao.model.RefreshToken;
import com.triador.yourwayserver.dao.model.User;
import com.triador.yourwayserver.dao.repo.RefreshTokenRepository;
import com.triador.yourwayserver.dto.Jwt;
import com.triador.yourwayserver.exception.AuthException;
import com.triador.yourwayserver.service.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.function.Function;

import static com.triador.yourwayserver.utils.Constants.SIGNING_KEY;

@Service
public class JwtTokenService implements Serializable {

    private JwtParser jwtParser;
    private UserService userService;
    private RefreshTokenRepository refreshTokenRepository;

    public JwtTokenService(JwtParser jwtParser,
                           UserService userService,
                           RefreshTokenRepository refreshTokenRepository) {
        this.jwtParser = jwtParser;
        this.userService = userService;
        this.refreshTokenRepository = refreshTokenRepository;
    }

    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    public String generateToken(User user) {

        Claims claims = Jwts.claims().setSubject(String.valueOf(user.getId()));
        claims.put("username", user.getEmail());
        claims.put("role", user.getRole());
        claims.put("userId", user.getId());

        return Jwts.builder()
                .setClaims(claims)
                .setIssuer("http://yourway.com")
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 10000))
                .signWith(SignatureAlgorithm.HS256, SIGNING_KEY)
                .compact();
    }

    public Jwt refreshAccessToken(Jwt jwtRequest) throws AuthException {
        String[] splittedRefreshTokenString = jwtRequest.getRefreshToken().split("\\.");
        String uid = splittedRefreshTokenString[0];
        String token = splittedRefreshTokenString[1];
        RefreshToken refreshToken = refreshTokenRepository.findByUid(Integer.valueOf(uid));
        System.out.println(token);
        System.out.println(refreshToken);
        if (!token.equals(refreshToken.getToken())) {
            refreshTokenRepository.deleteById(refreshToken.getId());
            throw new AuthException("alarm, unauthorized access!");
        } else {
            String newRefreshToken = RandomStringUtils.random(10, true, true);
            refreshToken.setToken(newRefreshToken);
            refreshTokenRepository.save(refreshToken);
        }

        Integer userId = jwtParser.extractUserId(jwtRequest.getAccessToken());

        User user = userService.findById(userId);
        String accessToken = generateToken(user);
        return new Jwt(accessToken, refreshToken.getUid() + "." + refreshToken.getToken());
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return (
                username.equals(userDetails.getUsername())
                        && !isTokenExpired(token));
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(SIGNING_KEY)
                .parseClaimsJws(token)
                .getBody();
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public String generateRefreshToken(Integer userId) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();
        String ip = request.getRemoteAddr();
        String userAgent = request.getHeader("User-Agent");
        String refreshTokenString = RandomStringUtils.random(10, true, true);
        LocalDateTime expiredAt = LocalDateTime.now().plusMonths(2);
        String uidString = RandomStringUtils.random(9, false, true);

        RefreshToken refreshToken = new RefreshToken();
        refreshToken.setIp(ip);
        refreshToken.setUserId(userId);
        refreshToken.setUid(Integer.valueOf(uidString));
        refreshToken.setToken(refreshTokenString);
        refreshToken.setUserAgent(userAgent);
        refreshToken.setExpiredAt(expiredAt);

        refreshTokenRepository.save(refreshToken);

        return uidString + "." + refreshTokenString;
    }

    @Transactional
    public Integer deleteRefreshToken(String refreshToken) {
        String[] splittedRefreshTokenString = refreshToken.split("\\.");
        String uid = splittedRefreshTokenString[0];
        return refreshTokenRepository.deleteByUid(Integer.valueOf(uid));
    }
}