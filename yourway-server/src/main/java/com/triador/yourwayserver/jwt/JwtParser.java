package com.triador.yourwayserver.jwt;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.triador.yourwayserver.exception.CommonException;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.TimeZone;

@Service
public class JwtParser {

    private ObjectMapper objectMapper;

    public JwtParser(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public Integer extractUserId(String accessToken) {
        JsonNode jsonNode = parseAndGetBody(accessToken);
        return jsonNode.get("userId").asInt();
    }

    public LocalDateTime extractExpirationTime(String accessToken) {
        JsonNode jsonNode = parseAndGetBody(accessToken);
        long expirationTimestamp = jsonNode.get("exp").asLong();
        return LocalDateTime.ofInstant(
                Instant.ofEpochSecond(expirationTimestamp),
                TimeZone.getDefault().toZoneId()
        );
    }

    public JsonNode parseAndGetBody(String accessToken) {
        String[] splittedToken = accessToken.split("\\.");
        String base64EncodedBody = splittedToken[1];
        Base64 base64Url = new Base64(true);
        String body = new String(base64Url.decode(base64EncodedBody));
        try {
            return objectMapper.readTree(body);
        } catch (IOException ex) {
            throw new CommonException("Can't parse access token", ex);
        }
    }
}

