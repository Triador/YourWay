package com.triador.yourwayserver.jwt;

import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityContext {

    public static TokenDetails getTokenDetails() {
        return (TokenDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
    }
}
