package com.triador.yourwayserver.jwt;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.triador.yourwayserver.utils.Constants.HEADER_STRING;
import static com.triador.yourwayserver.utils.Constants.TOKEN_PREFIX;

public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Resource(name = "userService")
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(HEADER_STRING);
        System.out.println(req.getRequestURL());
        String userId = null;
        String accessToken = null;
        if (header != null && header.startsWith(TOKEN_PREFIX)) {
            accessToken = header.replace(TOKEN_PREFIX, "");
            try {
                //todo think what to retrieve from accessToken in this case
                userId = jwtTokenService.getUsernameFromToken(accessToken);
            } catch (IllegalArgumentException e) {
                logger.error("an error occured during getting username from accessToken", e);
            } catch (ExpiredJwtException e) {
                logger.warn("the accessToken is expired and not valid anymore", e);
                req.setAttribute("expired", "accessToken is expired");
            } catch (SignatureException e) {
                logger.error("Authentication Failed. Username or Password not valid.");
            }
        } else {
            logger.warn("couldn't find bearer string, will ignore the header");
        }
        if (userId != null && SecurityContextHolder.getContext().getAuthentication() == null) {

            //todo think about this part and rewrite it here and in AuthenticationController
            UserDetails userDetails = userDetailsService.loadUserByUsername(userId);

            if (jwtTokenService.validateToken(accessToken, userDetails)) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails,
                        null, userDetails.getAuthorities());
                authentication.setDetails(createTokenDetails(accessToken));
                logger.info("authenticated user " + userId + ", setting security context");
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }

        chain.doFilter(req, res);
    }

    private TokenDetails createTokenDetails(String authToken) {
        return jwtTokenService.getClaimFromToken(authToken, claims -> {
            TokenDetails result = new TokenDetails();
            result.setUsername(claims.get("username", String.class));
            result.setUserId(claims.get("userId", Integer.class));
            result.setRole(claims.get("role", String.class));
            return result;
        });
    }
}
