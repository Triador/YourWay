package com.triador.yourwayserver.dao.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "vk_user")
public class VKUser implements SocialUser {

    @Id
    private Integer id;

    @Column(name = "user_id")
    private String userId;
    @Column
    private String name;
}
