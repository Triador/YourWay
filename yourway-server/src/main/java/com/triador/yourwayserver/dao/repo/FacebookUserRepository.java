package com.triador.yourwayserver.dao.repo;

import com.triador.yourwayserver.dao.model.FacebookUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FacebookUserRepository extends CrudRepository<FacebookUser, Integer>, SocialUserRepository {

    @Override
    @Query("SELECT u.id FROM FacebookUser u")
    Integer getIdByUserId(String userId);
}
