package com.triador.yourwayserver.dao.repo;

import com.triador.yourwayserver.dao.model.SocialUser;

public interface SocialUserRepository {

    boolean existsByUserId(String userId);

    Integer getIdByUserId(String userId);

    SocialUser save(SocialUser socialUser);
}
