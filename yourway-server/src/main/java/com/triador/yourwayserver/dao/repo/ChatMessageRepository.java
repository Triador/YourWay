package com.triador.yourwayserver.dao.repo;

import com.triador.yourwayserver.dao.model.Message;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ChatMessageRepository extends CrudRepository<Message, Integer> {

    List<Message> getAllByMarathonId(Integer marathonId);
}
