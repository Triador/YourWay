package com.triador.yourwayserver.dao.repo;

import com.triador.yourwayserver.dao.model.GoogleUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GoogleUserRepository extends CrudRepository<GoogleUser, Integer>, SocialUserRepository {

    @Override
    @Query("SELECT u.id FROM GoogleUser u")
    Integer getIdByUserId(String userId);
}
