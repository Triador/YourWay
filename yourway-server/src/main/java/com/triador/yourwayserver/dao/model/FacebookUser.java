package com.triador.yourwayserver.dao.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "facebook_user")
public class FacebookUser implements SocialUser {

    @Id
    private Integer id;

    @Column(name = "user_id")
    private String userId;
    @Column
    private String email;
    @Column
    private String name;
}
