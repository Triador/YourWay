package com.triador.yourwayserver.dao.repo;

import com.triador.yourwayserver.dao.model.VKUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface VKUserRepository extends CrudRepository<VKUser, Integer>, SocialUserRepository {

    @Override
    @Query("SELECT u.id FROM VKUser u")
    Integer getIdByUserId(String userId);
}
