package com.triador.yourwayserver.dao.model;

public interface SocialUser {

    Integer getId();

    void setId(Integer id);

    String getUserId();
}
