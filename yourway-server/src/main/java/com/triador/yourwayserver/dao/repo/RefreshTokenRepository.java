package com.triador.yourwayserver.dao.repo;

import com.triador.yourwayserver.dao.model.RefreshToken;
import org.springframework.data.repository.CrudRepository;

public interface RefreshTokenRepository extends CrudRepository<RefreshToken, Integer> {

    RefreshToken findByUid(Integer uid);

    Integer deleteByUid(Integer uid);
}
