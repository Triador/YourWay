package com.triador.yourwayserver.enumeration;

public enum BookStatus {

    UNLOCK,
    PROGRESS,
    FUTURE,
    FINISHED
}
