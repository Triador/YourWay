import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../models/user.model';
import { SignUpService } from './signup.service';
import { LoginService } from '../core/login.service';

@Component({
	selector: 'app-signup',
	templateUrl: './signup.component.html',
	styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
	user: User = new User();
	email: string;
	password: string;

	constructor(private router: Router, 
		private userService: SignUpService,
		private authService: LoginService) { }

	ngOnInit() {
		this.authService.setSignUp("false");
	}

	createUser(): void {
		this.user.email = this.email;
		this.user.password = this.password;
		this.userService.createUser(this.user)
		.subscribe( data => {
			alert("You are registered");
		});
	};

}
