import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

import {UserBook} from '../models/user-book.model';
import {Profile} from '../models/profile.model';

const USER_ID = 'USER_ID';
const url = 'http://localhost:8080/profiles';

@Injectable()
export class ProfileService {

  constructor(private http: HttpClient) {
  }

  setUserId(id: string) {
    window.localStorage.setItem(USER_ID, id);
  }

  getUserId(): number {
    return Number(window.localStorage.getItem(USER_ID));
  }

  addBookToProfile(id: number, bookStatus: string) {
    const userBook: UserBook = new UserBook();
    userBook.userId = this.getUserId();
    userBook.bookId = id;
    userBook.bookStatus = bookStatus;
    this.http.post(url, userBook).subscribe(response => console.log(response));
  }

  getProfile(userId: number) {
    return this.http.get<Profile>(url + '/' + userId);
  }

  deleteBook(id: number) {
    const userId: number = this.getUserId();
    return this.http.delete(url, {
      params: new HttpParams()
        .set('userId', userId.toString())
        .set('bookId', id.toString())
    });

  }
}
