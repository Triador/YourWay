import {Component, OnInit, Input} from '@angular/core';

import {TokenStorage} from '../core/token.storage';
import {ChatMessage} from '../models/chat-message.model';
import {ChatService} from './chat.service';

import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import * as jwtDecode from 'jwt-decode';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  @Input() marathonId: string;
  messages: string[] = [];
  disabled = true;
  text: string;
  private stompClient = null;

  constructor(private tokenStorage: TokenStorage,
              private chatService: ChatService) {
  }

  ngOnInit() {
    this.connect();
    console.log('get messages');
    this.chatService.getMessages(this.marathonId).subscribe(messages => {
      messages.forEach(message => {
        this.messages.push(message.dateSent + ': ' + message.text);
      });
      console.log('chat updated');
    });
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;
  }

  connect() {
    const socket = new SockJS('http://localhost:8080/ws');
    this.stompClient = Stomp.over(socket);

    const _this = this;
    this.stompClient.connect({}, function (frame) {
      _this.setConnected(true);
      console.log('Connected: ' + frame);

      _this.stompClient.subscribe('/topic/message', function (message) {
        const chatMessage: ChatMessage = JSON.parse(message.body);
        console.log(chatMessage);
        _this.showGreeting(chatMessage.dateSent + ': ' + chatMessage.text);
      });
    });
  }

  disconnect() {
    if (this.stompClient != null) {
      this.stompClient.disconnect();
    }

    this.setConnected(false);
    console.log('Disconnected!');
  }

  sendMessage() {
    const token = this.tokenStorage.getAccessToken();
    const decodedToken = jwtDecode(token);
    this.stompClient.send(
      '/chat/message',
      {},
      JSON.stringify({
        'marathonId': this.marathonId,
        'userId': decodedToken.userId,
        'username': decodedToken.username,
        'text': this.text,
        'dateSent': new Date()
      })
    );
  }

  showGreeting(message) {
    this.messages.push(message);
  }
}
