import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ChatMessage} from '../models/chat-message.model';

@Injectable()
export class ChatService {

  constructor(private http: HttpClient) {
  }

  getMessages(marathonId: string) {
    return this.http.get<ChatMessage[]>('http://localhost:8080/chat/messages/' + marathonId);
  }
}
