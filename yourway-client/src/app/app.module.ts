import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CustomMaterialModule} from './core/material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';
import {AppComponent} from './app.component';
import {BookComponent} from './book/book.component';
import {AppRoutingModule} from './core/app.routing.module';
import {BookService} from './book/book.service';
import {NoteService} from './book/note.service';
import {MarathonService} from './marathon/marathon.service';
import {SignUpService} from './signup/signup.service';
import {AllBookComponent} from './book/all-book.component';
import {LoginComponent} from './login/login.component';
import {ErrorDialogComponent} from './core/error-dialog.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {LoginService} from './core/login.service';
import {ProfileService} from './profile/profile.service';
import {Interceptor} from './core/inteceptor';
import {TokenStorage} from './core/token.storage';
import {SignupComponent} from './signup/signup.component';
import {SearchComponent} from './search/search.component';
import {MatListModule} from '@angular/material/list';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {ProfileComponent} from './profile/profile.component';
import {AddNoteDialogComponent} from './add-note-dialog/add-note-dialog.component';
import {AllMarathonComponent} from './marathon/all-marathon.component';
import {AddMarathonDialogComponent} from './add-marathon-dialog/add-marathon-dialog.component';
import {MarathonComponent} from './marathon/marathon.component';
import {SocialLoginModule, AuthServiceConfig} from 'angularx-social-login';
import {GoogleLoginProvider, FacebookLoginProvider} from 'angularx-social-login';
import {VKLoginProvider} from 'angularx-social-login-vk';
import {ChatComponent} from './chat/chat.component';
import {ChatService} from './chat/chat.service';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('761819174508-k9j2mga07it353skkp5ujs0ne03mmn9q.apps.googleusercontent.com')
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('276254446662583')
  },
  {
    id: VKLoginProvider.PROVIDER_ID,
    provider: new VKLoginProvider('6967458')
  }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    BookComponent,
    AllBookComponent,
    LoginComponent,
    ErrorDialogComponent,
    SignupComponent,
    SearchComponent,
    HeaderComponent,
    FooterComponent,
    ProfileComponent,
    AddNoteDialogComponent,
    AllMarathonComponent,
    AddMarathonDialogComponent,
    MarathonComponent,
    ChatComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CustomMaterialModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatListModule,
    FlexLayoutModule,
    SocialLoginModule
  ],
  entryComponents: [ErrorDialogComponent, AddNoteDialogComponent, AddMarathonDialogComponent],
  providers: [
    ErrorDialogComponent,
    BookService,
    LoginService,
    TokenStorage,
    SignUpService,
    SearchComponent,
    HeaderComponent,
    BookComponent,
    ProfileService,
    AddNoteDialogComponent,
    AddMarathonDialogComponent,
    NoteService,
    MarathonService,
    ChatService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    },
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
