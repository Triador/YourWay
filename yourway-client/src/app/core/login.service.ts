import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {Jwt} from '../models/jwt.model';

const LOGGED_IN = 'LOGGED_IN';
const SIGN_UP = 'SIGN_UP';

@Injectable()
export class LoginService {

  constructor(private http: HttpClient) {
  }

  setSignUp(value: string) {
    window.localStorage.setItem(SIGN_UP, value);
  }

  getSignUp() {
    return window.localStorage.getItem(SIGN_UP);
  }

  setLoggedIn(value: string) {
    window.localStorage.setItem(LOGGED_IN, value);
  }

  getLoggedIn() {
    return window.localStorage.getItem(LOGGED_IN);
  }

  logout(jwt: Jwt) {
    this.setLoggedIn('false');
    console.log(jwt.accessToken + ' ' + jwt.refreshToken);
    return this.http.post<any>('http://localhost:8080/auth/logout', jwt);
  }

  attemptAuth(email: string, password: string): Observable<any> {
    const credentials = {email: email, password: password};
    console.log('attemptAuth ::');
    return this.http.post<any>('http://localhost:8080/auth/login', credentials);
  }

  attemptGoogleAuth(token: string): Observable<any> {
    console.log('attemptGoogleAuth ::');
    return this.http.post<any>('http://localhost:8080/auth/google-login', {accessToken: token});
  }

  attemptFacebookAuth(token: string): Observable<any> {
    console.log('attemptFacebookAuth ::');
    return this.http.post<any>('http://localhost:8080/auth/facebook-login', {accessToken: token});
  }

  attemptVKAuth(token: string, userId: string): Observable<any> {
    console.log('attemptVKAuth ::');
    return this.http.post<any>('http://localhost:8080/auth/vk-login', {accessToken: token, userId: userId});
  }

  refreshAccessToken(jwt: Jwt) {
    return this.http.post<any>('http://localhost:8080/auth/refresh', {
      accessToken: jwt.accessToken,
      refreshToken: jwt.refreshToken
    });
  }
}
