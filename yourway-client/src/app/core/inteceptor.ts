import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {TokenStorage} from './token.storage';
import 'rxjs/add/operator/do';
import {LoginService} from './login.service';
import {BehaviorSubject, throwError} from 'rxjs';
import {catchError, filter, switchMap, take} from 'rxjs/operators';
import {Jwt} from '../models/jwt.model';

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class Interceptor implements HttpInterceptor {

  private refreshTokenInProgress = false;
  // Refresh Token Subject tracks the current token, or is null if no token is currently
  // available (e.g. refresh pending).
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
    null
  );

  constructor(private token: TokenStorage,
              private router: Router,
              private loginService: LoginService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authReq = req;
    const accessToken = this.token.getAccessToken();
    if (accessToken != null) {
      authReq = req.clone({headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + accessToken)});
    }
    return next.handle(authReq).pipe(
      catchError((error): Observable<any> => {
          console.log('catch error');
          // If error status is different than 401 we want to skip refresh token
          // So we check that and throw the error if it's the case
          if (error.status !== 401) {
            return throwError(error);
          }

          if (this.refreshTokenInProgress) {
            // If refreshTokenInProgress is true, we will wait until refreshTokenSubject has a non-null value
            // – which means the new token is ready and we can retry the request again
            return this.refreshTokenSubject.pipe(
              filter(result => result !== null),
              take(1),
              switchMap(subject => {
                return next.handle(this.getAuthRequestWithNewAuthorizationHeader(authReq));
              }));
          } else {
            this.refreshTokenInProgress = true;

            // Set the refreshTokenSubject to null so that subsequent API calls will wait until the new token has been retrieved
            this.refreshTokenSubject.next(null);

            // Call auth.refreshAccessToken(this is an Observable that will be returned)
            const jwtRequest = new Jwt(this.token.getAccessToken(), this.token.getRefreshToken());
            return this.loginService.refreshAccessToken(jwtRequest).pipe(
              switchMap(jwt => {
                this.token.saveAccessToken(jwt.accessToken);
                this.token.saveRefreshToken(jwt.refreshToken);
                // When the call to refreshToken completes we reset the refreshTokenInProgress to false
                // for the next time the token needs to be refreshed
                this.refreshTokenInProgress = false;
                this.refreshTokenSubject.next(jwt);

                return next.handle(this.getAuthRequestWithNewAuthorizationHeader(authReq));
              }),
              catchError(err => {
                console.log('CATCH!');
                this.refreshTokenInProgress = false;
                return throwError(err);
              }));
          }
        }
      ));
  }

  private getAuthRequestWithNewAuthorizationHeader(authRequest: HttpRequest<any>) {
    return authRequest.clone({
      headers: authRequest.headers.set(TOKEN_HEADER_KEY,
        'Bearer ' + this.refreshTokenSubject.getValue().accessToken)
    });
  }
}
