import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Note} from '../models/note.model';

const noteUrl = 'http://localhost:8080/notes/';

@Injectable()
export class NoteService {

  constructor(private http: HttpClient) {
  }

  saveNote(note: Note) {
    return this.http.post<Note>(noteUrl, note);
  }

  deleteNote(note: Note) {
    return this.http.delete(noteUrl + note.id);
  }
}
