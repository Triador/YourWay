import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';

import {Book} from '../models/book.model';
import {BookService} from './book.service';
import {Note} from '../models/note.model';
import {ProfileService} from '../profile/profile.service';
import {NoteService} from './note.service';
import {AddNoteDialogComponent} from '../add-note-dialog/add-note-dialog.component';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  book: Book = new Book();

  constructor(private bookService: BookService,
              private route: ActivatedRoute,
              private router: Router,
              private profileService: ProfileService,
              private noteService: NoteService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.bookService.getBook((params.get('id'))))
    ).subscribe(data => {
      console.log(data);
      data.imageLink = '../../assets/book_images/big_' + data.imageLink;
      this.book = data;
    });
  }

  addBookToProfile(bookStatus: string) {
    this.book.status = bookStatus;
    this.profileService.addBookToProfile(this.book.id, bookStatus);
  }

  openNoteDialog() {
    const dialogRef = this.dialog.open(AddNoteDialogComponent);

    dialogRef.afterClosed().subscribe(noteText => {
      if (noteText) {
        this.saveNote(noteText);
      }
    });
  }

  saveNote(noteText: string) {
    const note: Note = new Note();
    note.bookId = this.book.id;
    note.userId = this.profileService.getUserId();
    note.text = noteText;
    this.noteService.saveNote(note)
      .subscribe(data => {
        console.log(data);
        this.book.notes.push(data);
      });
  }

  deleteNote(index: number) {
    const note: Note = this.book.notes[index];
    this.noteService.deleteNote(note)
      .subscribe(data => {
        this.book.notes.splice(index, 1);
      });
  }

}
