import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {ShortBookDescription} from '../models/short-book-description.model';
import {BookService} from './book.service';

@Component({
  selector: 'app-all-book',
  templateUrl: './all-book.component.html',
  styleUrls: ['all-book.component.css']
})
export class AllBookComponent implements OnInit {
  books: ShortBookDescription[];

  constructor(private router: Router,
              private bookService: BookService) {
  }

  ngOnInit() {
    this.bookService.getBooks()
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          data[i].imageLink = '../../assets/book_images/small_' + data[i].imageLink;
        }
        this.books = data;
      });
  }

  openBook(id: number): void {
    this.router.navigate(['books/' + id]);
  }
}
