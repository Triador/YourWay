import {Note} from './note.model';

export class Book {
  id: number;
  title: string;
  author: string;
  pageAmount: number;
  publicationYear: number;
  isbn: string;
  description: string;
  imageLink: string;
  status: string;
  notes: Note[];
}
