import { Book } from './book.model';

export class Profile {
	email: string;
	imageLink: string;
	progressBooks: Book[];
	futureBooks: Book[];
	finishedBooks: Book[];
}