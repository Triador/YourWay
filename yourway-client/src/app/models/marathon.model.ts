export class Marathon {
  id: number;
  startDate: Date;
  endDate: Date;
  description: string;
}
