import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from '../core/login.service';
import {ProfileService} from '../profile/profile.service';
import {TokenStorage} from '../core/token.storage';
import {HeaderComponent} from '../header/header.component';
import {AuthService} from 'angularx-social-login';
import {FacebookLoginProvider, GoogleLoginProvider} from 'angularx-social-login';
import {LoginOpt, VKLoginProvider} from 'angularx-social-login-vk';

import * as jwtDecode from 'jwt-decode';

const vkLoginOptions: LoginOpt = {
  scope: 'email'
};

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string;
  password: string;

  constructor(private router: Router,
              private loginService: LoginService,
              private authService: AuthService,
              private token: TokenStorage,
              private header: HeaderComponent,
              private profileService: ProfileService) {
  }

  ngOnInit() {
    this.loginService.setSignUp('true');
    if (this.loginService.getLoggedIn() === 'true') {
      this.router.navigate(['books']);
    }
  }

  login(): void {
    this.loginService.attemptAuth(this.email, this.password).subscribe(
      data => {
        this.saveTokenAndNavigateToBooks(data);
      });
  }

  loginWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID)
      .then(user => {
        console.log(user);
        this.loginService.attemptGoogleAuth(user.idToken).subscribe(
          data => {
            this.saveTokenAndNavigateToBooks(data);
          }
        );
      });
  }

  loginWithFacebook(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID)
      .then(user => {
        console.log(user);
        this.loginService.attemptFacebookAuth(user.authToken).subscribe(
          data => {
            this.saveTokenAndNavigateToBooks(data);
          }
        );
      });
  }

  loginWithVK(): void {
    this.authService.signIn(VKLoginProvider.PROVIDER_ID, vkLoginOptions)
      .then(user => {
        console.log(user);
        this.loginService.attemptVKAuth(user.authToken, user.id).subscribe(
          data => {
            this.saveTokenAndNavigateToBooks(data);
          }
        );
      });
  }

  saveTokenAndNavigateToBooks(data): void {
    console.log(data);
    this.token.saveAccessToken(data.accessToken);
    this.token.saveRefreshToken(data.refreshToken);
    this.router.navigate(['books']);
    this.loginService.setLoggedIn('true');
    const decodedToken = jwtDecode(data.accessToken);
    console.log(decodedToken);
    this.profileService.setUserId(decodedToken.userId);
  }
}
