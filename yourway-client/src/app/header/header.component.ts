import {Component} from '@angular/core';
import {Router} from '@angular/router';

import {TokenStorage} from '../core/token.storage';
import {LoginService} from '../core/login.service';
import {ProfileService} from '../profile/profile.service';
import {Jwt} from '../models/jwt.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  constructor(private token: TokenStorage,
              private router: Router,
              private loginService: LoginService,
              private profileService: ProfileService) {
  }

  logout(): void {
    this.router.navigate(['login']);
    const jwt = new Jwt(this.token.getAccessToken(), this.token.getRefreshToken());
    this.token.signOut();
    this.loginService.logout(jwt).subscribe(data => console.log(data));
  }

  login(): void {
    this.router.navigate(['login']);
    this.loginService.setSignUp('true');
  }

  signup(): void {
    this.router.navigate(['signup']);
    this.loginService.setSignUp('false');
  }

  openProfile(): void {
    const userId = this.profileService.getUserId();
    console.log(userId);
    this.router.navigate(['profiles/' + userId]);
  }
}
